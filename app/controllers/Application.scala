package controllers

import play.api._
import play.api.mvc._
import play.api.libs.iteratee.Enumerator
import play.api.libs.concurrent.Execution.Implicits._
import scala.collection.JavaConversions._
import java.io._

object Application extends Controller {

  def index = Action {
    Ok(views.html.index("Your new application is ready."))
  }

  def list = Action { implicit request =>
  	var name = ""

  	try {
  		name = request.queryString("name").mkString
  	}catch {
  		case e: Exception => name = ""
  	}

  	val dir = new File("/downloads/"+name)

  	Ok(views.html.list(dir.listFiles))
  }

def download = Action { implicit request =>
	val name = request.queryString("name").mkString
  val file = new java.io.File("/downloads/"+name)
  val fileContent: Enumerator[Array[Byte]] = Enumerator.fromFile(file)    
    
  SimpleResult(
    header = ResponseHeader(200, Map(CONTENT_LENGTH -> file.length.toString)),
    body = fileContent
  )
}
}